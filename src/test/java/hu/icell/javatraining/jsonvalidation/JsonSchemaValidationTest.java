package hu.icell.javatraining.jsonvalidation;

import static org.junit.Assert.assertEquals;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class JsonSchemaValidationTest {

	
	private static Schema schema;
	
	@Rule
	public ExpectedException expected = ExpectedException.none();
	
	@BeforeClass
	public static void init() {
		
		JSONObject schemaAsJson = new JSONObject(
				new JSONTokener(JsonSchemaValidationTest.class.getResourceAsStream("/product_schema.json")));
		schema = SchemaLoader.load(schemaAsJson);
	}
	@Test
	public void testValidJsonObject() {

		JSONObject testObject = new JSONObject(
				new JSONTokener(JsonSchemaValidationTest.class.getResourceAsStream("/valid_json_object.json")));		
		schema.validate(testObject);
		
		assertEquals(testObject.getLong("id"), 1);
		assertEquals(testObject.get("name"), "smart lamppost");
		assertEquals(testObject.getLong("price"), 35500);
	}
	
	
	@Test
	public void testValidJsonObject2() {

		JSONObject testObject2 = new JSONObject();
		
		testObject2.put("id", 2);
		testObject2.put("name", "testObject2");
		testObject2.put("price", 23500);
		
		System.out.println(testObject2.toString());
		
		schema.validate(testObject2);
	}

	@Test
	public void testPriceMinimumValidationWithWrongJson() {
		
		expected.expect(ValidationException.class);
		expected.expectMessage("#/price: 0.0 is not higher than 0");
		JSONObject testObject = new JSONObject(
				new JSONTokener(JsonSchemaValidationTest.class.getResourceAsStream("/price_too_low_json_object.json")));

		schema.validate(testObject);
	}
}
